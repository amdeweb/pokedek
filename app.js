/*
 * File: app.js
 * Project: pokedex
 * Created: Sunday, March 21st 2021, 1:53:45 pm
 * Last Modified: Sunday, March 21st 2021, 2:43:28 pm
 * Copyright © 2021 AMDE Agência
 */

/**
 * requisição async, que reliaza a coleta de dados(get) sem a necessidade de refresh
 * o método fetch() retorna uma Promise
 * @param {*} id
 */
const getItem = (id) => {
  const API = `https://pokeapi.co/api/v2/pokemon/${id}`;
  fetch(API)
    .then((items) => items.json())
    .then((item) => console.log(item.name));
};

// getItem(25);

/**
 * coletar todos os pokemons e colocar numa lista
 * isso não é tão inteligente porque fazemos muitas requisições para o servidor
 * reduce() jeito fácil de transformar uma lista numa string, entre outras coisas
 * join() retorna todos os items do array concatenados e recebe como arg o separador
 * innerHTML é propriedade getter e setter
 */
const getAll = () => {
  const API = (id) => `https://pokeapi.co/api/v2/pokemon/${id}`;
  const ITEMS = [];

  for (let index = 1; index <= 150; index++) {
    ITEMS.push(fetch(API(index)).then((items) => items.json()));
  }

  Promise.all(ITEMS).then((items) => {
    const POKEMONS = items.reduce((acc, item) => {
      const TYPES = item.types.map((types) => types.type.name);
      acc += `
      <li class="card ${TYPES[0]}">
        <img class="card-image"
             alt="${item.name}" 
             src="https://pokeres.bastionbot.org/images/pokemon/${item.id}.png">
        <h2 class="card-title">${item.name}</h2>
        <p class="card-subtitle">${TYPES.join("|")}</p>
      </li>
      `;
      return acc;
    }, "");
    const UL = document.querySelector("[data-js='pokedex']");

    UL.innerHTML = POKEMONS;
  });
};

getAll();
